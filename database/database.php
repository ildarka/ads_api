<?php

    class Database
    {
        //блокировка методов создания экземпляров
        private function __construct(){}
        private function __clone(){}

        private static $conn = null;

        static public function getConnection()
        {
            if (is_null(self::$conn))
            {
                $connParams = parse_ini_file('config.ini');

                if($connParams === false)
                {
                    throw new Exception("Error reading database configuration file");
                }

                $connString = sprintf("pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s",
                    $connParams['host'],
                    $connParams['port'],
                    $connParams['database'],
                    $connParams['user'],
                    $connParams['password']);

                self::$conn = new PDO($connString);
                //self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            return self::$conn;
        }
    }