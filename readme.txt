Точка входа route.php

Примеры:
1. создать объявление
POST /api/ads
application/json; charset=UTF-8
{
   "title":"Nokia N8",
   "description":"Флагман 2011 года",
   "price":"5700",
   "images":[
      "h1222Fc2F8&usqp=CAU",
      "h2/ima2GriZ2uGLSlUr20jgGlzILVew&usqp=CAU",
      "2AU"
   ]
}

Возврат id в заголовке "Location"

2. запрос объявления по id
GET /api/ads/:id
-дополнить полями 
GET /api/ads/:id?fields=description,images

3. запрос объявлений (страницу, по умолчанию запрашивается первая)
GET /api/ads
-указать сортировку (убывание, возрастание)
GET /api/ads?sort=-price,create_date
-указать сортировку (возрастание)
GET /api/ads?sort=price
-запросить вторую страницу 
GET /api/ads?sort=price
-запросить вторую страницу для сортировки по убыванию цены
GET /api/ads?sort=-price&page=2

ЗЫ: не смог на винде поднять memcached, хотел заплюситься.