<?php

    include_once 'database/database.php';

    class AdsApi
    {
        protected $requestUri;
        protected $requestParams;
        protected $requestMethod;
        protected $dataId;
        protected $dataJson;

        public function __construct()
        {
            $this->requestMethod = $_SERVER['REQUEST_METHOD'];
            $this->requestUri = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
            $this->requestParams = $_REQUEST;
        }

        public function initialization()
        {
            $uri = $this->requestUri;
            $apiKey = (count($uri)>0 ? $uri[0] : '');
            $sectionKey = (count($uri)>1 ? strtok($uri[1],'?') : '');

            try {
                if ($apiKey === 'api') {
                    if ($sectionKey === 'ads') {
                        //создание объявления
                        if ($this->requestMethod === 'POST' && count($uri) === 2) {
                            $this->dataJson = json_decode(file_get_contents("php://input"), false, 512, JSON_UNESCAPED_UNICODE);
                            if (!empty($this->dataJson)) {
                                return $this->add();
                            } else {
                                return self::response(400);
                            };
                        };

                        //получение объявления
                        if ($this->requestMethod === 'GET' && count($uri) === 3) {
                            $this->dataId = (int)strtok($this->requestUri[2], '?');//,'/');
                            if ($this->dataId > 0) {
                                return $this->getOne($this->dataId);
                            };
                        };

                        //получение списка объявлений (страница)
                        if ($this->requestMethod === 'GET' && count($uri) === 2) {
                            return $this->getList();
                        };
                    };
                };
                return self::response(404);
            } catch (Exception $exception) {
                return self::response(500);
            };
        }

        private function add()
        {
            $jsonObj = $this->dataJson;

            //валидность данных
            if(!empty($jsonObj->title) && strlen(trim($jsonObj->title))<200
            && !empty($jsonObj->description) && strlen(trim($jsonObj->description))<=1000
            && !empty($jsonObj->price) && is_numeric($jsonObj->price) && (float) $jsonObj->price>0
            && count($jsonObj->images)<4) {

                $connection = Database::getConnection();
                $connection->beginTransaction();

                try {
                    $query = 'INSERT INTO ads 
                             (title, description, price)
                          VALUES
                             (:title, :description, :price)
                          RETURNING id';

                    $prepareQuery = $connection->prepare($query);
                    $prepareQuery->bindParam(':title', $jsonObj->title);
                    $prepareQuery->bindParam('description', $jsonObj->description);
                    $prepareQuery->bindParam(':price', $jsonObj->price);

                    $prepareQuery->execute();
                    $resultQuery = $prepareQuery->fetch();

                    $idAd = $resultQuery["id"];
                    foreach ($jsonObj->images as $imgUrl) {
                        $query = 'INSERT INTO ad_images_url  
                             (id_ad, url)
                          VALUES
                             (:id_ad, :url)';

                        $prepareQuery = $connection->prepare($query);
                        $prepareQuery->bindParam(':id_ad', $idAd);
                        $prepareQuery->bindParam(':url', $imgUrl);

                        $prepareQuery->execute();
                    }

                    $connection->commit();
                    return self::response(201,$this->requestMethod,$idAd);
                } catch (Exception $e) {
                    //не смогла бдэшка
                    $connection->rollBack();
                    return self::response(500,$this->requestMethod,0,$e->getMessage());
                }
            }
            else {
                //кривые параметры
                return self::response(400,$this->requestMethod);
            }
        }

        private function getOne($dataId)
        {
            $fieldsKeys = (array_key_exists('fields',$this->requestParams) ? explode(',', $this->requestParams['fields']) : []);

            try {
                $connection = Database::getConnection();

                $query = 'SELECT
                            ads.id,
                            ads.title,'
                            . (in_array('description', $fieldsKeys) ? 'ads.description, ' : '') .
                            'ads.price, 
                            ad_img.url mainImg 
                        FROM ads
                            LEFT JOIN ad_images_url ad_img
                                ON ads.id=ad_img.id_ad 
                                AND ad_img.id=(SELECT MIN(id) FROM ad_images_url img WHERE img.id_ad=ads.id)
                        WHERE
                            ads.id=?';

                $prepareQuery = $connection->prepare($query);
                $prepareQuery->execute([$dataId]);

                $resultQuery = $prepareQuery->fetchAll(PDO::FETCH_ASSOC);

                if(count($resultQuery) > 0) {
                    $resultQuery = $resultQuery[0];

                    if(array_key_exists('fields',$this->requestParams)) {
                        $query = 'SELECT
                                    url                                
                                FROM ad_images_url                                
                                WHERE
                                    id_ad=?';
                        $prepareQuery = $connection->prepare($query);
                        $prepareQuery->execute([$dataId]);

                        $resultQueryImg = $prepareQuery->fetchAll(PDO::FETCH_COLUMN);

                        $resultQuery += ['images'=>$resultQueryImg];
                    };

                    return self::response(200, $this->requestMethod, 0, $resultQuery);
                } else {
                    return self::response(401,$this->requestMethod);
                };
            } catch (Exception $e) {
                return self::response(500,$this->requestMethod);
            };
        }

        private function getList()
        {
            //записей на страницу
            $AdsCountOnPage = 10;
            //первая страница по умолчанию
            $pageIndex = (int)(array_key_exists('page',$this->requestParams) ? $this->requestParams['page'][0] : 1);

            //передан некорректный индекс
            if($pageIndex < 1) {
                return self::response(400,$this->requestMethod);
            } else {
                $sortKeys = (array_key_exists('sort',$this->requestParams) ? explode(',', $this->requestParams['sort']) : []);

                $sortStr = '';
                if($sortKeys !==' ') {
                    foreach ($sortKeys as $key) {
                        $sortDesc = (substr($key, 0, 1) === '-');
                        $key = ($sortDesc ? substr($key, 1, strlen($key)-1) : $key);

                        if (!in_array($key, ['create_date', 'price'])) {
                            return self::response(500, $this->requestMethod);
                        };

                        $sortStr = $sortStr . (strlen($sortStr) > 0 ? ', ' : ' ORDER BY ') . $key . ($sortDesc ? ' DESC' : ' ASC');
                    };
                };

                try {
                    $connection = Database::getConnection();

                    $query = 'SELECT COUNT(id) FROM ads';
                    $prepareQuery = $connection->prepare($query);
                    $prepareQuery->execute();

                    $adsCount = $prepareQuery->fetchAll(PDO::FETCH_COLUMN)[0];
                    $pagesCount = ceil($adsCount/10);

                    //нет страниц или такой нет
                    if($pagesCount === 0 || $pagesCount<$pageIndex) {
                        return self::response(404,$this->requestMethod);
                        die();
                    };

                    $response = array('pagination' => array('firstPage' => 1,
                                                            'currentPage'=> $pageIndex,
                                                            'prevPage' => ($pageIndex-1 < 1 ? 'null' : $pageIndex-1),
                                                            'nextPage' => ($pageIndex+1 > $pagesCount ? 'null' : $pageIndex+1),
                                                            'lastPage' => $pagesCount));

                    $query = 'SELECT
                                ads.id,
                                ads.title,
                                ad_img.url mainImg, 
                                ads.price
                            FROM ads
                                LEFT JOIN ad_images_url ad_img
                                    ON ads.id=ad_img.id_ad 
                                    AND ad_img.id=(SELECT MIN(id) FROM ad_images_url img WHERE img.id_ad=ads.id)'
                            .$sortStr.' 
                            LIMIT ? OFFSET ?';

                    $prepareQuery = $connection->prepare($query);
                    $prepareQuery->execute([$AdsCountOnPage,$AdsCountOnPage*($pageIndex-1)]);

                    $resultQuery = $prepareQuery->fetchAll(PDO::FETCH_ASSOC);

                    if(count($resultQuery)>0) {
                        $response += array('data' => $resultQuery);
                    };

                    return self::response(200, $this->requestMethod, 0, $response);
                } catch (Exception $e) {
                    return self::response(500,$this->requestMethod,0, $e->getMessage());
                };
            };
        }

        public static function response($statusCode = 200, $method = 'POST, GET', $dataID = 0, $data = '')
        {
            $responseStates = array(
                    '200' => 'OK',
                    '201' => 'Created',

                    '400' => 'Bad Request',
                    '404' => 'Not Found',

                    '500' => 'Internal Server Error');

            header('HTTP/1.1 ' . $statusCode . ' ' . $responseStates[$statusCode]);

            header('Access-Control-Allow-Origin: *');

            if($method<>"") {
                header('Access-Control-Allow-Methods: ' . $method);
            };

            header('Content-Type: application/json; charset=UTF-8');
            header('Access-Control-Max-Age: 3000');

            if($dataID>0) {

                header('Location: /ads/'.$dataID);
            };

            if($data !== "") {
                header('Cache-Control: no-store, no-cache, must-revalidate');
                return json_encode($data,JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
            };
        }

    }