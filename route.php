<?php

    include_once 'ads-api/ads-api.php';

    //точка входа
    try {
        $adsApi = new AdsApi();
        echo $adsApi->initialization();
    } catch (Exception $exception) {
        echo AdsApi::response(500);
    }